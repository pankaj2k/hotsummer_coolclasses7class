import { Hotsummercoolclasses1Page } from './app.po';

describe('hotsummercoolclasses1 App', function() {
  let page: Hotsummercoolclasses1Page;

  beforeEach(() => {
    page = new Hotsummercoolclasses1Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
